﻿namespace Hive_Management
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.assignJobButton = new System.Windows.Forms.Button();
            this.shiftsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.workerBeeJobComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.reportTextBox = new System.Windows.Forms.TextBox();
            this.nextShiftButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shiftsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.assignJobButton);
            this.groupBox1.Controls.Add(this.shiftsNumericUpDown);
            this.groupBox1.Controls.Add(this.workerBeeJobComboBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allocation work to workers";
            // 
            // assignJobButton
            // 
            this.assignJobButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.assignJobButton.Location = new System.Drawing.Point(6, 69);
            this.assignJobButton.Name = "assignJobButton";
            this.assignJobButton.Size = new System.Drawing.Size(309, 29);
            this.assignJobButton.TabIndex = 5;
            this.assignJobButton.Text = "Assign this work to a bee";
            this.assignJobButton.UseVisualStyleBackColor = true;
            this.assignJobButton.Click += new System.EventHandler(this.assignJobButton_Click);
            // 
            // shiftsNumericUpDown
            // 
            this.shiftsNumericUpDown.Location = new System.Drawing.Point(206, 43);
            this.shiftsNumericUpDown.Name = "shiftsNumericUpDown";
            this.shiftsNumericUpDown.Size = new System.Drawing.Size(109, 21);
            this.shiftsNumericUpDown.TabIndex = 4;
            // 
            // workerBeeJobComboBox
            // 
            this.workerBeeJobComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.workerBeeJobComboBox.FormattingEnabled = true;
            this.workerBeeJobComboBox.Items.AddRange(new object[] {
            "Collecting nectar",
            "Production of honey",
            "Egg care",
            "Teaching bees",
            "Keeping care of the hive",
            "Patrol with stings"});
            this.workerBeeJobComboBox.Location = new System.Drawing.Point(6, 42);
            this.workerBeeJobComboBox.Name = "workerBeeJobComboBox";
            this.workerBeeJobComboBox.Size = new System.Drawing.Size(194, 23);
            this.workerBeeJobComboBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Task of worker";
            // 
            // reportTextBox
            // 
            this.reportTextBox.Location = new System.Drawing.Point(12, 122);
            this.reportTextBox.Multiline = true;
            this.reportTextBox.Name = "reportTextBox";
            this.reportTextBox.Size = new System.Drawing.Size(458, 181);
            this.reportTextBox.TabIndex = 1;
            // 
            // nextShiftButton
            // 
            this.nextShiftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nextShiftButton.Location = new System.Drawing.Point(340, 12);
            this.nextShiftButton.Name = "nextShiftButton";
            this.nextShiftButton.Size = new System.Drawing.Size(130, 104);
            this.nextShiftButton.TabIndex = 6;
            this.nextShiftButton.Text = "Work through the next shift";
            this.nextShiftButton.UseVisualStyleBackColor = true;
            this.nextShiftButton.Click += new System.EventHandler(this.nextShiftButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 315);
            this.Controls.Add(this.nextShiftButton);
            this.Controls.Add(this.reportTextBox);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Hive Management";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shiftsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button assignJobButton;
        private System.Windows.Forms.NumericUpDown shiftsNumericUpDown;
        private System.Windows.Forms.ComboBox workerBeeJobComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox reportTextBox;
        private System.Windows.Forms.Button nextShiftButton;
    }
}

