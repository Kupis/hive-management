﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hive_Management
{
    public partial class MainForm : Form
    {
        private Queen queen;

        public MainForm()
        {
            InitializeComponent();

            workerBeeJobComboBox.SelectedIndex = 0;

            Worker[] workers = new Hive_Management.Worker[4];
            workers[0] = new Worker(new string[] { "Collecting nectar", "Production of honey" }, 175);
            workers[1] = new Worker(new string[] { "Egg care", "Teaching bees" }, 114);
            workers[2] = new Worker(new string[] { "Keeping care of the hive", "Patrol with stings" }, 149);
            workers[3] = new Worker(new string[] { "Collecting nectar", "Production of honey", "Egg care", "Teaching bees", "Keeping care of the hive", "Patrol with stings" }, 155);
            queen = new Queen(workers, 275);
        }

        private void assignJobButton_Click(object sender, EventArgs e)
        {
            if (queen.AssignWork(workerBeeJobComboBox.Text, (int)shiftsNumericUpDown.Value) == false)
                MessageBox.Show("There are bot any available bees to complete the task '" + workerBeeJobComboBox.Text + "'", "Queen of bees speaks...");
            else
                MessageBox.Show("Task '" + workerBeeJobComboBox.Text + "' will be completed in " + shiftsNumericUpDown.Value + " shifts", "Queen of bees speaks...");
        }

        private void nextShiftButton_Click(object sender, EventArgs e)
        {
            reportTextBox.Text = queen.WorkTheNextShift();
        }
    }
}
