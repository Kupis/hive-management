﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hive_Management
{
    public class Queen : Bee
    {
        public Queen(Worker[] workers, double weightMg)
            :base(weightMg)
        {
            this.workers = workers;
        }

        private Worker[] workers;
        private int shiftNumber = 0;

        public bool AssignWork(string job, int numberOfShifts)
        {
            for (int i = 0; i < workers.Length; i++)
                if (workers[i].DoThisJob(job, numberOfShifts))
                    return true;
            return false;
        }

        public string WorkTheNextShift()
        {
            double honeyConsumed = HoneyConsumptionRate();

            shiftNumber++;
            string report = "Shift raport number " + shiftNumber + "\r\n";
            for (int i = 0; i < workers.Length; i++)
            {
                honeyConsumed += workers[i].HoneyConsumptionRate();

                if (workers[i].DidYouFinish())
                    report += "Worker number " + (i + 1) + " ended his task\r\n";
                if (String.IsNullOrEmpty(workers[i].CurrentJob))
                    report += "Worker number " + (i + 1) + " is not working\r\n";
                else
                    if (workers[i].ShiftsLeft > 0)
                    report += "Worker number " + (i + 1) + " will do '" + workers[i].CurrentJob
                        + "' for " + workers[i].ShiftsLeft + " shifts\r\n";
                    else
                    report += "Worker number " + (i + 1) + " will end '" + workers[i].CurrentJob + "' after this shift\r\n";
            }

            report += "Total honey consumption: " + honeyConsumed + " units\r\n";

            return report;
        }
    }
}
